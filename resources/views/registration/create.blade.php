@extends('layout')

@section('content')

	<div class="col-sm-8">
		
		<h1>Register</h1>

		<form method="POST" action="/register">

		{{ csrf_field() }}

		<div class="form-group">
			
			<label for="name">Name:</label>
			<input class="form-control" type="text" id="name" name="name" required />

		</div>

		<div class="form-group">
			
			<label for="email">Email:</label>
			<input class="form-control" type="email" id="email" name="email" required />

		</div>

		<div class="form-group">
			
			<label for="password">Password:</label>
			<input class="form-control" type="password" id="password" name="password" required />

		</div>

		<div class="form-group">
			
			<label for="password_confirmation">Confirm Password:</label>
			<input class="form-control" type="password" id="password_confirmation" name="password_confirmation" />

		</div>

		<div class="form-group">
			
			<button class="btn btn-primary" type="submit" >Register</button>

		</div>

		</form>

		@include('errors')

	</div>


@endsection
