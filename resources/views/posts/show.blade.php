@extends('layout')

@section('content')

	<div class="col-md-8 blog-main">

		<h1>{{ $post->title }}</h1>
		<p>{{ $post->body }}</p>

		<hr>
		<div class="comment">
			
			<ul class="list-group">
			@foreach($post->comments as $comment)

			<li class="list-group-item">
				<b>{{ $comment->created_at->diffForHumans() }}</b> - 
				{{ $comment->body }}

			</li>
			@endforeach
			</ul>
		</div>

		<hr>

		<div class="card">
			
			<div class="card-block">
				
				<form method="POST" action="/posts/{{ $post->id }}/comments">
					{{ csrf_field() }}
					<div class="form-group">
						
						<textarea name="body" placeholder="enter your comment here" class="form-control"></textarea>

					</div>

					<div class="form-group">
						
						<button type="Submit" class="btn btn-primary">Submit</button>
					</div>

				</form>

			</div>

		</div>

		<div class="alert danger">
			
			<ul>
				
				@foreach($errors->all() as $error)
				<li>
					
					{{ $error }}
				</li>

				@endforeach

			</ul>

		</div>

	</div>

@endsection