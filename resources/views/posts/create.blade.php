@extends('layout')


@section('content')

	 <div class="col-md-8 blog-main">
	<h1>Create a post</h1>

		<form method="POST" action="/posts">

			{{ csrf_field() }}

  <div class="form-group">
    <label for="inputTitle">Title</label>
    <input type="text" class="form-control" name="inputTitle" id="inputTitle" aria-describedby="" placeholder="Enter title">
  </div>

  <div class="form-group">
    <label for="inputBody">Body</label>
    <textarea id="inputBody" name="inputBody" class="form-control"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Publish</button>
</form>

	<div class="form-group">
		
		<ul>
			
			@foreach($errors->all() as $error)
			<li>{{	$error	}}</li>
			@endforeach

		</ul>

	</div>

	</div>
@endsection