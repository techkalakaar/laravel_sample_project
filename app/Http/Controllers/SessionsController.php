<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Auth;
use App\User;

class SessionsController extends Controller
{

	public function __construct(){
		$this->middleware('guest', ['except'=>'destroy']);
	}

    public function create(){
    	return view('sessions.create');
    }

    public function destroy(){
    	auth()->logout();

    	return redirect()->home();
    }

    public function store(){

        //dd(request(['email','password']));
        \DB::connection()->enableQueryLog();
    	//attempt to authenticate user
    	/*if(!auth()->attempt(request(['email','password']))){
    		return back()->withErrors([
    			'message' => 'Please check your credentials'
    		]);
    	}*/


        /*if(\Auth::attempt(['email'=>request('email'), 'password'=>request('password')])){

            
    	   return redirect()->home();

        }*/

        $user = User::whereEmail(request('email'))->wherePassword(request('password'))->first();
        if($user) {
            //dd("true");
            \Auth::loginUsingId($user->id);
        }
        //$queries = \DB::getQueryLog();
            //dd($queries);

        return back()->withErrors([
            'message' => 'Please check your credentials'
        ]);

    	//auth()->attempt(request(['email','password']));
    }
}
