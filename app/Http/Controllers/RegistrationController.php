<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class RegistrationController extends Controller
{
    public function create(){

    	return view('registration.create');
    }

    public function store(){

    	//Validation
    	$this->validate(request(), [
    		'name' => 'required',
    		'email' => 'required|email',
    		'password' => 'required|confirmed'
    	]);

    	//store
    	$user = User::create([
    		'name' => request('name'),
    		'email' => request('email'),
    		'password' => request('password')
    	]);

    	//sign in

    	auth()->login($user);

    	//Return

    	return redirect()->home();

    }
}
