<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Comment;

use App\Repositories\Posts;

use Carbon\Carbon;

class PostController extends Controller
{

    public function __construct(){
        $this->middleware('auth')->except(['index','show']);
    }

    public function index(Posts $posts){
        \DB::connection()->enableQueryLog();
    	//$posts = Post::all();
    	//$posts = Post::latest();
    	//$posts = Post::orderBy('created_at', 'desc')->get();

        $posts = $posts->all();

        //$posts = (new \App\Repositories\Posts)->all();

        /*if(request('month') && request('year')){
            $posts = Post::latest()
            ->filter(request(['month','year']))
            ->get();
        }
        else{
            $posts = Post::latest()->get();
        }*/


        /*$posts = Post::latest();

        if($month = request('month')){
            $posts->whereMonth('created_at', 3);
        }

        if($year = request('year')){
            $posts->whereYear('created_at', $year);
        }

        $posts = $posts->get();*/

        //$archives = Post::archives();

        

        //$queries = \DB::getQueryLog();
        //dd($queries);

        //return $archives;

    	return view('posts.index', compact('posts'));
    }

    public function show(Post $post){

    	//$post = Post::find($id);

    	return view('posts.show', compact('post'));
    }

    public function create(){
    	return view('posts.create');
    }

    public function store(){

    	$this->validate(request(),[
    		'inputTitle' => 'required',
    		'inputBody' => 'required'
    	]);

    	//$posts = new Post;

    	// $posts->title = request('inputTitle');

    	//dd(request('inputTitle'));

    	// $posts->body = request('inputBody');

    	// $posts->save();

        /*auth()->user()->publish(
            new Post(request(['inputTitle','inputBody']))
        );*/

    	Post::create([
    		'title' => request('inputTitle'),
    		'body' => request('inputBody'),
            'user_id' => auth()->id()
    	]);

    	return redirect('/posts');
    	//dd(request(['inputTitle', 'inputBody']));
    }

    

}
