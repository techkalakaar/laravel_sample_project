<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/',function(){
	return view('welcome');
})->name('home');

Route::get('tasks', 'TasksController@index');

/*Route::get('/tasks', function(){

	$name = 'Jigar';
	$age = 26;

	$task = [

		'exhibit',

		'thetechy',
	
		'thewheelz'

	];

	//$task = DB::table('tasks')->get();

	//return $task;

	$task = App\Task::all();

	return view('tasks/index', compact('task'));

	//return view('about', compact('name', 'age'));

	//return view('about')->with('name' => $name);

});*/

Route::get('/tasks/{task}','TasksController@show');

Route::get('/posts/','PostController@index');
Route::get('/posts/create', 'PostController@create');

Route::post('/posts', 'PostController@store');
Route::get('/posts/{post}', 'PostController@show');

Route::POST('/posts/{post}/comments','CommentsController@store');

Route::get('/register','RegistrationController@create');
Route::post('/register','RegistrationController@store');
Route::get('/login','SessionsController@create');
Route::post('/login','SessionsController@store');
Route::get('/logout','SessionsController@destroy');
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
